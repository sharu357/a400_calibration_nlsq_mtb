%% MATLAB toolbox optimization for parameter estimation of ASYSTR400

clear 
close all
clc

%% 1.Setting parameter and nominal values

l0 = sqrt(2)*305;
l1 = 240;
l2 = 315;
l3 = 240;
l4 = 315;
delta1 = deg2rad(0);
delta2 = deg2rad(0);
xC = 305/sqrt(2);
yC = 305/sqrt(2);
deltaC = deg2rad(0);

parId = [l0 l1 l2 l3 l4 delta1 delta2 xC yC deltaC];
nPar = length(parId);
parTol = [0.5 0.5 0.5 0.5 0.5 deg2rad(1) deg2rad(1) 0.5 0.5 deg2rad(1)];    %Half-range of the parameters values

parAct = parId  + [0.2 -0.15 -0.1 0.3 0.1 deg2rad(0.23) deg2rad(-0.15) ...  %Assumed actual parameters for simulation
                   0.35 -0.4 deg2rad(0.23)];

%% 2.Generating test points and corresponding joint angles

pT = GenTp([25,50,75,100,125,150,175],16,0);                                %Test points are generated on concentric circles 
jaT = zeros(size(pT));                                                      
nPT = length(pT);
for i = 1:nPT                                                               %Corresponding joint angles are computed for the ideal model
    jaT(i,:) = IKCal(pT(i,:),parId);
end

%% 3.Simulating FARO readings
 
pFARO = zeros(size(pT));
for i = 1:nPT
    pFARO(i,:) = FKCal(jaT(i,:),parAct);
end 

% Insert code to read actual measured data from FARO

%% Non-linear least squares

options = optimoptions('lsqnonlin');
options.FunctionTolerance = 10^-20;
option.OptimalityTolerance = 10^-20;
option.StepTolerance = 10^-20;

lsf = @(x)FKOpt(jaT,x)-pFARO;

parOpt = lsqnonlin(lsf,parId);
acr = abs(parOpt - parAct);                                                 %Applicable only during simulation