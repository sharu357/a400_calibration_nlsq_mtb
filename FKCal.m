%% Forward Kinematics for the 2-D model of Asystr400
function[X] = FKCal(ja,par)
%% Importing the parameters of the robot
l0 = par(1);
l1 = par(2);
l2 = par(3);
l3 = par(4);
l4 = par(5);
delta1 = par(6);
delta2 = par(7);
xC = par(8);
yC = par(9);
deltaC = par(10);

%% Joint Angles as inputs
theta1 = ja(1);
theta2 = ja(2);

%% Phi1: Solving at^2 + bt + c = 0
a = l0^2 + l1^2 -l2^2 + l3^2 + l4^2 -2*l0*l3*cos(delta1 + theta1) - 2*l1*l3*cos(delta1 -delta2 + theta1 -theta2) + 2*l0*l1*cos(delta2 + theta2) + 2*l4*(l0 - l3*cos(delta1 + theta1) + l1*cos(delta2 + theta2));
b = 4*l4*(l3*sin(delta1 + theta1) - l1*sin(delta2 + theta2));
c = l0^2 + l1^2 -l2^2 + l3^2 + l4^2 -2*l0*l3*cos(delta1 + theta1) - 2*l1*l3*cos(delta1 -delta2 + theta1 -theta2) + 2*l0*l1*cos(delta2 + theta2) - 2*l4*(l0 - l3*cos(delta1 + theta1) + l1*cos(delta2 + theta2));

t = solQuad(a,b,c); %(-b + Discriminant)/2a a.k.a the plus root
phi1 = 2*atan(t(1));

%% Position of the tool tip in the base frame
x = -xC + l3*cos(delta1 + theta1) + l4*cos(phi1);
y = -yC + l3*sin(delta1 + theta1) + l4*sin(phi1);

x = x*cos(deltaC) - y*sin(deltaC);
y = x*sin(deltaC) + y*cos(deltaC);

X = [x,y];
    
end
