% Note that the function does not run if joint angles are not defined

function p = FKOpt(ja,par)
n = length(ja);
p = zeros(n,2);
for i = 1:n
    p(i,:) = FKCal(ja(i,:),par);
end
end

