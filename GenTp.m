function tp = GenTp(rad,n,ang)
c = length(rad);    %Number of concentric circles
tp = zeros(c*n,2);  %Total test points equals the number of circles times the number of points on each
d = 360/n;          %Angle between adjacent points on a circle
for i = 1:c
    for j = 1:n
        tp(j+n*(i-1),:) = rad(i)*[cos(deg2rad(ang+d*j)), sin(deg2rad(ang+d*j))];
    end
end
end